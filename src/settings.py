import os

# data pipeline

DATA_FOLDER = os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))), 'data')

INPUT_DATA = os.path.join(DATA_FOLDER, '0_input')
RAW_DATA = os.path.join(DATA_FOLDER, '1_raw')
PREPROCESSED_DATA = os.path.join(DATA_FOLDER, '2_preprocessed')
OUTPUT_DATA = os.path.join(DATA_FOLDER, '3_output')