
import pandas as pd
import os
from datetime import timedelta
import datetime
import numpy as np
from sklearn.linear_model import Lasso, LogisticRegression, LinearRegression
from sklearn.feature_selection import SelectFromModel
from sklearn.preprocessing import StandardScaler, LabelEncoder
from sklearn import preprocessing

from src.settings import INPUT_DATA, RAW_DATA, PREPROCESSED_DATA, OUTPUT_DATA
from src.Combine_Stations_CBS import combine_stations_cbs

""" Script om de incidenten data te verrijken met externe databronnen:
    1. CBS data
    2. Kalender met vakantie en feestdagen
    3. ....

    output: Nog in te vullen
"""

def select_relevant_variables(cbs_station, df_incidenten_full):
    """ Selecteer de belangrijke CBS variabelen op basis van een Lasso regressie
        De lasso regressie wordt gedaan op een maand gemiddelde van het aantal incidenten op een station en de
        cbs variabelen. De reden voor een maand gemiddelde is dat de cbs gegevens statisch zijn per station en je
        daarom ook 1 statisch cijfer wil voor de incidenten. Er is anders geen relatie te leggen.
        De incidenten data wordt geagrregeerd per maand per station, en vervolgens wordt per station het gemiddeld aantal incidenten
        per maand berekend.

                Input:
                df_incidenten_full, cbs_station

                Returns:
                cbs_station_relevant
        """

    df_incidenten_full['Melding_maand'] = pd.to_datetime(df_incidenten_full['Melding_dtm'],format='%Y-%m-%d').dt.month
    df_incidenten_full['Melding_jaar'] = pd.to_datetime(df_incidenten_full['Melding_dtm'], format='%Y-%m-%d').dt.year
    df_incidenten_full['Melding_jaarmaand'] = df_incidenten_full['Melding_jaar'].astype(str) + "-" + df_incidenten_full['Melding_maand'].astype(str)

    # Aggregeer incidenten naar maand - locatie
    groupby_columns = ['Melding_jaarmaand', 'Locatie_cde', 'Locatie_nam']
    aggregations = {'incident': 'sum'}
    df_month_aggr = df_incidenten_full.groupby(by=groupby_columns).agg(aggregations).reset_index()

    # Aggregeer gemiddelde incidenten per maand van een locatie
    groupby_columns = ['Locatie_cde', 'Locatie_nam']
    aggregations = {'incident': 'mean'}
    df_station_aggr = df_month_aggr.groupby(by=groupby_columns).agg(aggregations).reset_index()

    # Merge station gemiddelde incidenten en cbs op basis van locatie cde
    df_station_aggr_cbs = pd.merge(df_station_aggr, cbs_station, on=['Locatie_cde'], how='left')



    # Selecteer x en y voor input lasso regressie
    x_variables = df_station_aggr_cbs.iloc[:, 4:]
    y = df_station_aggr_cbs.incident

    # y variabele bevat floats, mag alleen ints bevatten dus encoden
    lab_enc = preprocessing.LabelEncoder()
    y_encoded = lab_enc.fit_transform(y)


    # Maak scaler aan om x variabelen te scalen
    scaler = StandardScaler()
    scaler.fit(x_variables.fillna(0))


    # Lasso regressie
    select_from_model = SelectFromModel(LogisticRegression(C=1, penalty='l1', solver='liblinear', tol=0.00001))

    select_from_model.fit(scaler.transform(x_variables.fillna(0)), y_encoded)

    selected_feat = x_variables.columns[(select_from_model.get_support())]
    not_selected_feat = x_variables.columns[~(select_from_model.get_support())]
    print('Selected CBS features: ', selected_feat)
    print('Niet selected CBS features: ', not_selected_feat)

    cbs_station_relevant = cbs_station[['Locatie_cde'] + list(selected_feat)]


    '''''
    model = LinearRegression().fit(scaler.transform(x_variables.fillna(0)), y)
    r_sq = model.score(scaler.transform(x_variables.fillna(0)), y)
    print('coefficient of determination:', r_sq)
    print('intercept:', model.intercept_)
    print('slope:', model.coef_)
    '''''

    return cbs_station_relevant

def  add_cbs_data(df_incidenten_full, run_source_cbs=True, run_koppel_cbs=True, save=True):

    """ De CBS gegevens worden toegevoegd aan de geaggregeerde incidenten data dmv join op Locatie_cde.
        Zie script Combine_Stations_CBS voor het koppelen van CBS gegevens aan een station
        D.m.v. een lasso regressie wordt bepaald welke CBS variabelen belangrijk zijn om mee te nemen in het model

            Input:
            df_incidenten_full

            Returns:
            df_incidenten_cbs
    """

    if run_source_cbs:
        cbs_station = combine_stations_cbs()
    else:
        cbs_station = pd.read_csv(os.path.join(PREPROCESSED_DATA, 'CBS/cbs_station.csv'), sep=';')

    if run_koppel_cbs:
        # Selecteer op basis van lasso regressie de belangrijskte variabelen
        cbs_station_relevant = select_relevant_variables(cbs_station, df_incidenten_full)

        # Merge incidenten en cbs op basis van locatie cde
        df_incidenten_cbs = pd.merge(df_incidenten_full, cbs_station_relevant, on=['Locatie_cde'], how='left')
        print(df_incidenten_cbs.head())

        if save:
            df_incidenten_cbs.to_csv(os.path.join(PREPROCESSED_DATA, 'Datasources/incidenten_cbs_1.csv'), sep=';', index=False)
    else:
        df_incidenten_cbs = pd.read_csv(os.path.join(PREPROCESSED_DATA, 'Datasources/incidenten_cbs_1.csv'),sep=';')

    return df_incidenten_cbs

def add_kalender_data(df_incidenten_cbs, run=True, save=True):

    """ De kalender gegevens met vakantie/feestdagen worden toegevoegd aan de geaggregeerde incidenten data dmv join op Regio.
        De kalender bevat vakanties met start en eind datum, deze moet eerst uitgeklapt worden voordat deze gekoppeld kan worden
        Na het uitklappen van de kalender, groepeer de kalender bij datum en tel het aantal events dat plaatsvindt op die datum
        Waarde kan 0 (geen vakantie/feestdag), 1 (alleen vakantie óf feestdag) of 2 (vakantie én feestdag) zijn

        Een hulptabel met station->vakantieregio wordt gebruikt om de vakanties aan stations te koppelen

            Input:
            df_incidenten_cbs

            Returns:
            df_incidenten_cbs_kal
    """

    if run:
        # Lees kalender en vakantieregio-station in
        kalender = pd.read_csv(os.path.join(INPUT_DATA, 'Kalender/Kalender.csv'), sep=';')
        station_vakantieregio = pd.read_csv(os.path.join(INPUT_DATA, 'Kalender/Station_vakantieregio.csv'), sep=';', encoding='latin-1')
        station_vakantieregio['Locatie_cde'] = station_vakantieregio['Locatie_cde'].str.capitalize()

        #Join vakantieregio op incidenten data per station
        df_incidenten_cbs = pd.merge(df_incidenten_cbs, station_vakantieregio[['Locatie_cde','Vakantie_Regio']], on='Locatie_cde', how='left')

        # Filter vakantie Bouwvak er uit, is overlappend met zomervakantie en deze laatste is langer
        kalender = kalender[kalender.Naam != 'Bouwvak']

        # Kalender bevat datums met start en eind, om deze informatie te kunnen joinen klappen we de kalender eerst uit met alle datums
        # Maak date formats
        kalender['start_date'] = pd.to_datetime(kalender['Begin datum'], format='%d-%m-%Y').dt.date
        kalender['end_date'] = pd.to_datetime(kalender['Eind datum'], format='%d-%m-%Y').dt.date
        df_incidenten_cbs['Melding_dtm'] = pd.to_datetime(df_incidenten_cbs['Melding_dtm'], format='%Y-%m-%d').dt.date


        # Maak kolom met alleen de datums
        datums_incidenten = pd.DataFrame(df_incidenten_cbs['Melding_dtm'].unique())
        datums_incidenten.rename(columns={0: 'Melding_dtm'}, inplace=True)

        # Maak key om te joinen
        kalender = kalender.assign(key=1)
        datums_incidenten = datums_incidenten.assign(key=1)

        # Join datums op kalender en filter op de juiste rijen, dit resulteert in een uitgeklapte kalender
        kalender = pd.merge(kalender, datums_incidenten, on='key').drop('key', axis=1)
        kalender = kalender.query('Melding_dtm >= start_date and Melding_dtm <= end_date')

        # Groepeer de kalender bij datum en tel het aantal events dat plaatsvindt
        # Waarde 1 (alleen vakantie óf feestdag) of 2 (vakantie én feestdag) zijn
        kalender = kalender.groupby(by=['Melding_dtm', 'Vakantie_Regio']).agg({'Naam': 'count'}).reset_index()
        kalender.rename(columns={'Naam': 'vakantie_feestdag'}, inplace=True)

        # Vul bij na waarde 0 in want dan geen vakantie/feestdag,
        df_incidenten_cbs_kal = pd.merge(df_incidenten_cbs, kalender, on=['Melding_dtm', 'Vakantie_Regio'], how='left')
        df_incidenten_cbs_kal['vakantie_feestdag'] = df_incidenten_cbs_kal['vakantie_feestdag'].fillna(0)

        if save:
            df_incidenten_cbs_kal.to_csv(os.path.join(PREPROCESSED_DATA, 'Datasources/incidenten_cbs_kal.csv'), sep=';', index=False)
    else:
        df_incidenten_cbs_kal = pd.read_csv(os.path.join(PREPROCESSED_DATA, 'Datasources/incidenten_cbs_kal.csv'),sep=';')

    return df_incidenten_cbs_kal


def add_evenementen_data(df_incidenten_cbs_kal, run=True, save=True):

    """ Evenementen data toevoegen dmv join op Melding_dtm, Melding_tijd_2uur en Locatie_nam
        Evenementen date heeft een regel per evenement en station, met hierin een begin en eind datum. Voor de join een regel per datum creeeren.

            Input:
            df_incidenten_cbs_kal

            Returns:
            df_incidenten_cbs_kal_event
    """

    if run:
        # Evenementen data inlezen
        evenementen = pd.read_csv(os.path.join(INPUT_DATA, 'Evenementen/Evenementenoverzicht.csv'), sep=';', encoding='latin-1')

        evenementen['Locatie_cde'] = evenementen['Locatie_cde'].str.capitalize()

        # Bestand bevat dubbele regels, alleen unieke regels overhouden
        evenementen = evenementen.drop_duplicates()

        # Evenementen zonder datum verwijderen
        evenementen = evenementen.dropna()

        # Filter evenementen eruit die geannuleerd zijn
        evenementen = evenementen[evenementen.Status != 'Geannuleerd']

        # Evenementen data bevat een rij per evenement en station met hierin een begin en eind datum, eerst een rij per datum creeeren
        # date format
        evenementen['start_date_pre'] = pd.to_datetime(evenementen['Begin datum'], format='%d-%m-%Y').dt.date
        evenementen['end_date_pre'] = pd.to_datetime(evenementen['Eind datum'], format='%d-%m-%Y').dt.date
        df_incidenten_cbs_kal['Melding_dtm'] = pd.to_datetime(df_incidenten_cbs_kal['Melding_dtm'], format='%Y-%m-%d').dt.date

        #Bandbreedte van een uur aan evenementen toevoegen
        evenementen['Begintijd_timeformat'] = pd.to_datetime(evenementen.Begintijd, format='%H:%M:%S').dt.time
        evenementen['Eindtijd_timeformat'] = pd.to_datetime(evenementen.Eindtijd, format='%H:%M:%S').dt.time

        evenementen['Begin_datumtijd'] = evenementen.apply(lambda row: datetime.datetime.combine(row['start_date_pre'], row['Begintijd_timeformat']) - timedelta(hours=1), axis=1)
        evenementen['Eind_datumtijd'] = evenementen.apply(lambda row: datetime.datetime.combine(row['end_date_pre'], row['Eindtijd_timeformat']) if row['Eindtijd'][0:2] == '23' else datetime.datetime.combine(row['end_date_pre'], row['Eindtijd_timeformat']) + timedelta(hours=1), axis=1)


        evenementen['start_date'] = evenementen['Begin_datumtijd'].dt.date
        evenementen['end_date'] = evenementen['Eind_datumtijd'].dt.date

        evenementen['Begintijd_bandbreedte'] = evenementen['Begin_datumtijd'].dt.time
        evenementen['Eindtijd_bandbreedte'] = evenementen['Eind_datumtijd'].dt.time


        # maak een kolom met alleen de datums
        datumtijd_incidenten = pd.DataFrame(df_incidenten_cbs_kal[['Melding_dtm', 'Melding_tijd_2uur']]).drop_duplicates()
        #datumtijd_incidenten.rename(columns={0: 'Melding_dtm'}, inplace=True)

        datumtijd_incidenten['Melding_tijd_2uur'] = pd.to_datetime(datumtijd_incidenten.Melding_tijd_2uur, format='%H:%M:%S').dt.time

        datumtijd_incidenten['Melding_datumtijd'] = datumtijd_incidenten.apply(
            lambda row: datetime.datetime.combine(row['Melding_dtm'], row['Melding_tijd_2uur']), axis=1)

        # Maak key om te joinen
        evenementen = evenementen.assign(key=1)
        datumtijd_incidenten = datumtijd_incidenten.assign(key=1)

        # Join datums op evenementen en filter op de juiste rijen, dit geeft een regel per evenement en station per datum
        # Let op: als evenement 09:00 begint dan wordt ie pas mee geteld in uurblok 10:00 (tot 12:00)
        evenementen = pd.merge(evenementen, datumtijd_incidenten, on='key').drop('key', axis=1)
        evenementen = evenementen.query('Melding_dtm >= start_date and Melding_dtm <= end_date '
                                        'and Melding_tijd_2uur >= Begintijd_bandbreedte and Melding_tijd_2uur <= Eindtijd_bandbreedte')


        evenementen = evenementen.drop(['Evenement'], axis=1).drop_duplicates(keep=False)

        evenementen = evenementen[['Locatie_cde', 'Melding_dtm', 'Melding_tijd_2uur', 'Categorie', 'Logistiek', 'Service', 'Veiligheid', 'Risicocategorie']]
        evenementen_final = evenementen.rename(columns={'Categorie': 'Evenement_cat'
                                                        , 'Logistiek': 'Logistiek_risico_cat', 'Service': 'Service_risico_cat'
                                                        ,'Veiligheid': 'Veiligheid_risico_cat', 'Risicocategorie': 'Risico_cat'})

        evenementen_final.Evenement_cat = evenementen_final.Evenement_cat.str.capitalize()
        evenementen_final.Logistiek_risico_cat = evenementen_final.Logistiek_risico_cat.str.capitalize()
        evenementen_final.Service_risico_cat = evenementen_final.Service_risico_cat.str.capitalize()
        evenementen_final.Veiligheid_risico_cat = evenementen_final.Veiligheid_risico_cat.str.capitalize()

        evenementen_final.Risico_cat = np.where(evenementen_final.Risico_cat=='Geen score', -1, evenementen_final.Risico_cat)

        evenementen_final.Evenement_cat = np.where(evenementen_final.Evenement_cat.str.contains('Beurs'), 'Beurs', evenementen_final['Evenement_cat'])



        ## LET OP: nu nooit 2 evenementen op het zelfde station op dezelfde datum. Later iets inbouwen voor het geval dat dit wel voorkomt?
        # Creating instance of labelencoder
        labelencoder = LabelEncoder()

        evenementen_final['Veiligheid_risico_cat'] = np.where((evenementen_final['Veiligheid_risico_cat'] == '0')
                                                              | (evenementen_final['Veiligheid_risico_cat'] == 0)
                                                              | (evenementen_final.Veiligheid_risico_cat.isnull()), 'Geen_risico',
                                                              evenementen_final['Veiligheid_risico_cat'])
        evenementen_final['Veiligheid_risico_cat_num'] = labelencoder.fit_transform(evenementen_final['Veiligheid_risico_cat'].astype(str))


        evenementen_final['Evenement_cat'] = np.where((evenementen_final['Evenement_cat'] == '0')
                                                      | (evenementen_final['Evenement_cat'] == 0),'Geen',
                                                      evenementen_final['Evenement_cat'])
        dummy_evenement_cat = pd.get_dummies(evenementen_final['Evenement_cat'])

        evenementen_final['Risico_cat'] = np.where(evenementen_final['Risico_cat'] == 'ntb', 0, evenementen_final['Risico_cat']).astype(int)

        evenementen_final = pd.concat([evenementen_final, dummy_evenement_cat],axis=1)

        groupby_columns = ['Locatie_cde', 'Melding_dtm', 'Melding_tijd_2uur']
        aggregations = {'Evenement_cat': '-'.join,
                        'Veiligheid_risico_cat': '-'.join,
                        'Veiligheid_risico_cat_num': 'max',
                        'Risico_cat': 'max',
                        'Beurs': 'sum',
                        'Concert': 'sum',
                        'Cultuur': 'sum',
                        #'Dance': 'sum',
                        'Festival': 'sum',
                        #'Hardcore': 'sum',
                        'Special': 'sum',
                        'Sport': 'sum',
                        'Volks': 'sum'
                        }
        evenementen_final_grouped = evenementen_final.groupby(by=groupby_columns).agg(aggregations).reset_index()



        # Merge incidenten en evenementen op basis van Melding_dtm, Melding_tijd_2uur en Locatie_nam
        ########## Gaat nog niet goed : Landelijk moet nog aan alles gekoppeld worden
        df_incidenten_cbs_kal_event = pd.merge(df_incidenten_cbs_kal, evenementen_final_grouped, on=['Locatie_cde', 'Melding_dtm', 'Melding_tijd_2uur'], how='left').fillna(0)

        if save:
            df_incidenten_cbs_kal_event.to_csv(os.path.join(PREPROCESSED_DATA, 'Datasources/incidenten_cbs_kal_event.csv'),sep=';', index=False)
    else:
        df_incidenten_cbs_kal_event = pd.read_csv(os.path.join(PREPROCESSED_DATA, 'Datasources/incidenten_cbs_kal_event.csv'), sep=';')
    return df_incidenten_cbs_kal_event

def add_scholendata(df_incidenten_cbs, run=True):

    if run:
        # Scholen data inlezen
        scholen_data = pd.read_csv(os.path.join(INPUT_DATA, 'Scholen/Scholen_data.csv'), sep=';', encoding='latin-1')
        scholen_data['Gemeente'] = scholen_data['Gemeente'].str.capitalize()

        # Station - Gemeente inlezen
        gemeente_station_hulptabel = pd.read_csv(os.path.join(INPUT_DATA, 'Stations/gemeente_station_hulptabel.csv'), sep=';', encoding='latin-1')
        gemeente_station_hulptabel['Gemeente'] = gemeente_station_hulptabel['Gemeente'].str.capitalize()
        gemeente_station_hulptabel['Locatie_cde'] = gemeente_station_hulptabel['Locatie_cde'].str.capitalize()

        scholen_data_station = pd.merge(scholen_data, gemeente_station_hulptabel[['Locatie_cde', 'Gemeente']], on=['Gemeente'], how='right')
        scholen_data_station = scholen_data_station.drop(scholen_data_station.columns[[0,1]], axis=1)

        df_incidenten_cbs = pd.merge(df_incidenten_cbs, scholen_data_station,on=['Locatie_cde'], how='left')

        df_incidenten_cbs['leerlingen_scholen_opgeteld'] = df_incidenten_cbs.voortgezet_onderwijs + df_incidenten_cbs.basisonderwijs\
                                                            + df_incidenten_cbs.speciaal_basisonderwijs + df_incidenten_cbs.speciale_scholen\
                                                            + df_incidenten_cbs.beroepsbegeleidende_leerweg + df_incidenten_cbs.beroepsopleidende_leerweg\
                                                            + df_incidenten_cbs.hoger_beroepsonderwijs + df_incidenten_cbs.wetenschappelijk_onderwijs


        df_incidenten_cbs.to_csv(os.path.join(PREPROCESSED_DATA, 'Datasources/df_incidenten_cbs_2.csv'),sep=';', index=False)

    else:
        df_incidenten_cbs = pd.read_csv(os.path.join(PREPROCESSED_DATA, 'Datasources/df_incidenten_cbs_2.csv'),sep=';')

    return df_incidenten_cbs


def combine_datasources(df_incidenten_full):
    """
        Deze functie voegt aan de incidenten data externe databronnen toe:
        1. CBS data
        2. Kalender met vakantie en feestdagen
        3. Evenementen

            Input:
            df_incidenten_full, CBS data, Kalender, Evenementen

            Returns:
            df_incidenten_cbs_kal_event
    """
    # 1. CBS data
    #df_incidenten_cbs = add_cbs_data(df_incidenten_full, run_source_cbs=False, run_koppel_cbs=True)
    #df_incidenten_cbs = add_scholendata(df_incidenten_cbs, run=True)

    # 2. Kalender met vakantie en feestdagen
    #df_incidenten_cbs_kal = add_kalender_data(df_incidenten_cbs, run=True)

    df_incidenten_cbs_kal = pd.read_csv(os.path.join(PREPROCESSED_DATA, 'Datasources/incidenten_cbs_kal.csv'), sep=';')
    # 3. Evenementen
    df_incidenten_cbs_kal_event = add_evenementen_data(df_incidenten_cbs_kal, run=True)
    
    return df_incidenten_cbs_kal_event


if __name__ == "__main__":

    #df_incidenten_full = pd.read_csv(os.path.join(PREPROCESSED_DATA, 'Incidenten/4_Incidenten_full_lags.csv'), sep=';')
    combine_datasources(df_incidenten_full=2)