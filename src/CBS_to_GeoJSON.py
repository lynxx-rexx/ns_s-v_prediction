import os
import geopandas as gpd


def save_geojson(folder, filename, gdf):
    # Store the geopandas to GeoJSON format in folder RAW\state:
    file_export = os.path.join(folder, filename)
    if not os.path.exists(folder):
        os.makedirs(folder)
    print('Exporting to: ' + file_export)
    # GM_NAAM & BU_NAAM can have special characters which is why the utf-8 enconding is necessary
    gdf.to_file(file_export, driver='GeoJSON', encoding='utf-8')  # als aternatief (iso-8859-1) # utf-8

def process_shape(file_path_in, folder_out, filename_out, save = True):
    print('opening shape: {}'.format(file_path_in))
    gdf = gpd.read_file(file_path_in)

    # removing rows which have no geometry --> otherwise the functions don't work!
    gdf_not_null = gdf[gdf['geometry'].notnull()].copy()

    # Store the geopandas to GeoJSON format in folder RAW\state:
    if save:
        folder = folder_out
        # filename = shape_file[:-4] + '_' + str(tolerance) + '_.geojson'
        filename = filename_out
        save_geojson(folder, filename, gdf_not_null)

def cbs_to_geojson():
    folder_in = '../data/0_input/CBS2017'
    filename_in = 'CBS_PC4_2017_v2.shp'
    file_path_in = os.path.join(folder_in, filename_in)
    folder_out ='../data/1_raw/CBS2017'
    filename_out = 'CBS_PC4_2017_v2.geojson'
    file_path_out = os.path.join(folder_out, filename_out)

    process_shape(file_path_in, folder_out, filename_out, save=True)

    return

if __name__ == "__main__":
    cbs_to_geojson()



