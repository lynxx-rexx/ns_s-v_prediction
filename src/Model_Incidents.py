import pandas as pd
import os
from datetime import timedelta
import numpy as np

from imblearn.ensemble import BalancedRandomForestClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import GridSearchCV, TimeSeriesSplit
from sklearn.metrics import accuracy_score, confusion_matrix, roc_curve, roc_auc_score, precision_score, recall_score, precision_recall_curve, auc
import matplotlib.pyplot as plt
from sklearn.preprocessing import LabelEncoder
import pylab as pl


from src.settings import INPUT_DATA, RAW_DATA, PREPROCESSED_DATA, OUTPUT_DATA

''' Settings '''
pd.set_option('display.max_columns', None)
pd.set_option('display.expand_frame_repr', False)
pd.set_option('max_colwidth', None)

def encode_category_variables(df_incidenten_datasources):
    """
        Sommige variabelen bevatten tekst, deze moeten worden omgezet naar een category
        met numerieke waardes voor logit model

            Input:
            df_incidenten_datasources

            Returns:
            df_incidenten_datasources
    """

    # Creating instance of labelencoder
    labelencoder = LabelEncoder()


    # Maak dummy variabelen
    #dummy_locaties = pd.get_dummies('locatie_' + df_incidenten_datasources['Locatie_cde'])
    dummy_2uur = pd.get_dummies('uurblok_'+ df_incidenten_datasources['Melding_tijd_2uur'].str[:2])
    dummy_weekdag = pd.get_dummies(df_incidenten_datasources['weekdag'])
    dummy_maand = pd.get_dummies(df_incidenten_datasources['Melding_maand'])
    dummy_jaar = pd.get_dummies(df_incidenten_datasources['Melding_jaar'])
    #dummy_locatie = pd.get_dummies('locatie_' + df_incidenten_datasources['Locatie_cde'])
    dummy_prorailtype = pd.get_dummies(df_incidenten_datasources['Prorailtype'])
    dummy_risico_index = pd.get_dummies('ri_' + df_incidenten_datasources['Risico_index_cde'])


    # Voeg alle dummies samen aan data
    df_incidenten_datasources = pd.concat([df_incidenten_datasources, dummy_2uur, dummy_weekdag
                                              , dummy_maand, dummy_jaar, dummy_prorailtype, dummy_risico_index], axis=1)

    # Assigning numerical values and storing in another column
    df_incidenten_datasources['Locatie_cde_num']= labelencoder.fit_transform(df_incidenten_datasources['Locatie_cde'].astype(str))
    #df_incidenten_datasources['Logistiek_risico_cat_num'] = labelencoder.fit_transform(df_incidenten_datasources['Logistiek_risico_cat'].astype(str))
    #df_incidenten_datasources['Service_risico_cat_num'] = labelencoder.fit_transform(df_incidenten_datasources['Service_risico_cat'].astype(str))
    df_incidenten_datasources['Veiligheid_risico_cat_num'] = labelencoder.fit_transform(df_incidenten_datasources['Veiligheid_risico_cat'].astype(str))
    df_incidenten_datasources['Bemenst_Station_ind'] = labelencoder.fit_transform(df_incidenten_datasources['Bemenst_Station_ind'].astype(str))
    df_incidenten_datasources['Buitenland_ind'] = labelencoder.fit_transform(df_incidenten_datasources['Buitenland_ind'].astype(str))

    #df_incidenten_datasources.to_csv(os.path.join(PREPROCESSED_DATA, 'Datasources/df_incidenten_datasources_qlik.csv'),sep=';', index=False, decimal=',')

    return df_incidenten_datasources


def split_data(df_incidenten_datasources):
    """
        Data wordt gesplitst in train, validatie en testset

            Input:
            df_incidenten_binary

            Returns:
            trainset, validationset, testset
    """

    train_split_frac = 0.7

    # Sorteer datums en maak kolom met alleen alle datums aan
    df_incidenten_datasources['Melding_dtm'] = pd.to_datetime(df_incidenten_datasources['Melding_dtm'], format='%Y-%m-%d').dt.date
    df_incidenten_datasources = df_incidenten_datasources.sort_values(by=['Melding_dtm'], axis=0)
    datums = df_incidenten_datasources['Melding_dtm'].unique()

    # Selecteer datums voor de trainset, validationset en testset
    # Selecteer allereerst datums voor train-validation en testset
    test_split = int(train_split_frac * len(datums))
    train_val_datums = datums[:test_split]
    test_datums = datums[test_split:]

    # Maak train en testset
    trainset = df_incidenten_datasources[df_incidenten_datasources['Melding_dtm'].isin(train_val_datums)]
    testset = df_incidenten_datasources[df_incidenten_datasources['Melding_dtm'].isin(test_datums)]

    return trainset, testset

def run_logistic_regression(trainset, testset, save=True):
    """
            Weighted Logistic Regression model

                Input:
                trainset, validationset

                Returns:
                Nog in te vullen
        """

    # Sorteer datums
    trainset['Melding_dtm'] = pd.to_datetime(trainset['Melding_dtm'], format='%Y-%m-%d').dt.date
    trainset = trainset.sort_values(by=['Melding_dtm'], axis=0)

    #----------------------------------------------------------------------------------------------------------------
    # Crossvalidation

    '''''
    # Definieer weight hyperparameter
    # !!!!!!!!!!!!!!!!!!!!!!!!!(werkt nog niet met gridsearch want cv doet random split en dit is timeserie) -> Nu wel
    w = [{0: 1000, 1: 100}, {0: 1000, 1: 10}, {0: 1000, 1: 1.0},
         {0: 500, 1: 1.0}, {0: 400, 1: 1.0}, {0: 300, 1: 1.0}, {0: 200, 1: 1.0},
         {0: 150, 1: 1.0}, {0: 100, 1: 1.0}, {0: 99, 1: 1.0}, {0: 10, 1: 1.0},
         {0: 0.01, 1: 1.0}, {0: 0.01, 1: 10}, {0: 0.01, 1: 100},
         {0: 0.001, 1: 1.0}, {0: 0.005, 1: 1.0}, {0: 1.0, 1: 1.0},
         {0: 1.0, 1: 0.1}, {0: 10, 1: 0.1}, {0: 100, 1: 0.1},
         {0: 10, 1: 0.01}, {0: 1.0, 1: 0.01}, {0: 1.0, 1: 0.001}, {0: 1.0, 1: 0.005},
         {0: 1.0, 1: 10}, {0: 1.0, 1: 99}, {0: 1.0, 1: 100}, {0: 1.0, 1: 150},
         {0: 1.0, 1: 200}, {0: 1.0, 1: 300}, {0: 1.0, 1: 400}, {0: 1.0, 1: 500},
         {0: 1.0, 1: 1000}, {0: 10, 1: 1000},
         {0: 100, 1: 1000}, {0: 0.01, 1: 0.99}, {0: 1.0, 1.0:120.0}
         ]
    hyperparam_grid = {"class_weight": w}

    # Definieer model
    model_logit = LogisticRegression(random_state=13)

    # Maak kolom met alleen alle datums aan
    datums = trainset['Melding_dtm'].unique()

    # Definieer evaluatie procedure
    cv_datums_test = TimeSeriesSplit(n_splits=100)
    grid_test = GridSearchCV(model_logit, hyperparam_grid, scoring="roc_auc", cv=cv_datums_test, n_jobs=-1, refit=True)

    cv_datums = [[train, test] for train, test in TimeSeriesSplit(n_splits=2).split(datums)]

    cv_timeserie = list()

    for [train, test] in cv_datums:
        train_datums = datums[train]
        val_datums = datums[test]
        train_index = np.where(trainset['Melding_dtm'].isin(train_datums))[0].tolist()
        test_index = np.where(trainset['Melding_dtm'].isin(val_datums))[0].tolist()
        train_test = [train_index, test_index]
        cv_timeserie.append(train_test)


    grid = GridSearchCV(model_logit, hyperparam_grid, scoring="roc_auc", cv=cv_timeserie, n_jobs=-1, refit=True)
    grid.fit(x, y)
    print(f'Best score: {grid.best_score_} with param: {grid.best_params_}')

    #resultaat RB:Best score: 0.83559837798675 with param: {'class_weight': {0: 1.0, 1: 1000}}
    '''''

    # ----------------------------------------------------------------------------------------------------------------

    # ,'Beurs', 'Concert','Cultuur','Dance','Festival','Geen','Hardcore','Special','Sport','Volks'
    #,'incident_last1week','incident_last2week','incident_last3week','incident_last4week'
    drop_columns = ['Melding_dtm', 'Melding_tijd_2uur', 'Melding_jaarmaand', 'Locatie_nam', 'aantal_incidenten', 'incident',
                     'Melding_dtm_last1week', 'Melding_dtm_last2week', 'Melding_dtm_last3week', 'Melding_dtm_last4week',
                     'Vakantie_Regio', 'Locatie_cde', 'weekdag', 'Evenement_cat', 'Veiligheid_risico_cat',
                     'Melding_maand', 'Melding_jaar', 'Risico_cat', 'gem_incidenten_per_dag', 'speciaal_basisonderwijs',
                     'basisonderwijs', 'speciale_scholen', 'leerlingen_scholen_opgeteld', 'INW_014', 'INW_2544', 'INW_4564',
                     'TOTHH_EENP', 'TOTHH_MPZK', 'HH_EENOUD', 'HH_TWEEOUD', 'WON_HCORP',
                     'hoger_beroepsonderwijs', 'wetenschappelijk_onderwijs','voortgezet_onderwijs', 'beroepsopleidende_leerweg',
                     'beroepsbegeleidende_leerweg', 'INW_1524', 'WONING','UITKMINAOW','P_NW_MIG_A','WOZWONING','P_HUURWON'
                     ,'AV5_HOTEL','AV3_RESTAU','AFS_OPRIT','AFS_TREINS',  'Risico_index_cde','Prorailtype', 'Locatie_cde_num'
                    ,'incident_last4week', 'Veiligheid_risico_cat_num'
                    ]

    columns_in_model = trainset.drop(drop_columns, axis=1)

    for col in columns_in_model.columns:
        print(col)

    x = trainset.drop(drop_columns, axis=1).values
    y = trainset['incident'].values
    x_test = testset.drop(drop_columns, axis=1).values
    y_test = testset['incident'].values


    # ----------------------------------------------------------------------------------------------------------------
    # Final model

    # Optimal weights
    # w = grid.best_params_
    # w_optimal = {0: 1.0, 1: 1000}

    # Definieer final model
    #logit_model_final = LogisticRegression(random_state=13, class_weight='balanced') # 'balanced' w_optimal
    tree_model = BalancedRandomForestClassifier(n_estimators=10, n_jobs=-1, bootstrap=True, max_features='sqrt', random_state=18) #

    # Fit final model
    #logit_model_final.fit(x, y)
    tree_model.fit(x, y)

    # # Feature importance
    # importances = tree_model.feature_importances_
    # std = np.std([tree.feature_importances_ for tree in tree_model.estimators_],axis=0)
    # indices = np.argsort(importances)
    # names = [x_variabelen.columns[i] for i in indices]
    #
    # # Plot the feature importances of the forest
    # plt.figure()
    # plt.title("Feature importances")
    # plt.barh(range(x_variabelen.shape[1]), importances[indices],
    #          color="r", xerr=std[indices], align="center")
    # # If you want to define your own labels,
    # # change indices to a list of labels on the following line.
    # plt.yticks(range(x_variabelen.shape[1]), names)
    # plt.ylim([-1, x_variabelen.shape[1]])
    # plt.show()

    # Voorspel testset
    y_pred = tree_model.predict(x_test)
    y_pred_prob = tree_model.predict_proba(x_test)[:,1]

    # Maak panda dataframe kolommen van voorspellingen
    y_pred = pd.DataFrame(y_pred, columns=['voorspelling_incident'])
    y_pred_prob = pd.DataFrame(y_pred_prob, columns=['voorspelling_kans'])

    # Voeg voorspellingen toe aan testset
    testset.reset_index(drop=True, inplace=True)
    y_pred.reset_index(drop=True, inplace=True)
    y_pred_prob.reset_index(drop=True, inplace=True)
    voorspellingen = pd.concat([testset,y_pred, y_pred_prob], axis=1)






    # ----------------------------------------------------------------------------------------------------------------
    # Check performance

    # check scriptie: https://science.vu.nl/en/Images/stageverslag-graauw_tcm296-414225.pdf
    # hier gebruiken ze op pagina 29 een absolute hits performance meausure
    print(f'Accuracy Score: {accuracy_score(y_test, y_pred)}')
    print(f'Confusion Matrix: \n{confusion_matrix(y_test, y_pred)}')
    print(f'Area Under Curve: {roc_auc_score(y_test, y_pred)}')
    print(f'Recall score: {recall_score(y_test, y_pred)}')

    y_pred_tilburg = voorspellingen[voorspellingen.Locatie_cde=='Tb'].voorspelling_incident
    y_test_tilburg = voorspellingen[voorspellingen.Locatie_cde=='Tb'].incident
    y_pred_prob_tilburg = voorspellingen[voorspellingen.Locatie_cde=='Tb'].voorspelling_kans


    #print(f'Tilburg Accuracy Score: {accuracy_score(y_test_tilburg, y_pred_tilburg)}')
    print(f'Tilburg Confusion Matrix: \n{confusion_matrix(y_test_tilburg, y_pred_tilburg)}')
    #print(f'Tilburg Area Under Curve: {roc_auc_score(y_test_tilburg, y_pred_tilburg)}')
    #print(f'Tilburg Recall score: {recall_score(y_test_tilburg, y_pred_tilburg)}')



    # ----------------------------------------------------------------------------------------------------------------
    # Bepaal optimale cutoffpoint

    threshold = Find_Optimal_Cutoff(voorspellingen['incident'], voorspellingen['voorspelling_kans'])
    print('Optimal cutt-of:', threshold[0])

    # Find prediction to the dataframe applying threshold
    voorspellingen['voorspelling_incident_optimal'] = np.where(voorspellingen['voorspelling_kans'] > threshold[0], 1, 0)


    # Print confusion Matrix
    confusion_matrix(voorspellingen['incident'], voorspellingen['voorspelling_incident_optimal'])
    print(f'Optimal cutt-of Confusion Matrix: \n{confusion_matrix(voorspellingen.incident, voorspellingen.voorspelling_incident_optimal)}')

    y_pred_optm_tilburg = voorspellingen[voorspellingen.Locatie_cde == 'Tb'].voorspelling_incident_optimal
    print(f'Tilburg Confusion Matrix: \n{confusion_matrix(y_test_tilburg, y_pred_optm_tilburg)}')


    if save:
        voorspellingen.to_csv(os.path.join(OUTPUT_DATA, 'voorspellingen.csv'), sep=';', index=False)

    # Bepaal optimale cutoffpoint (grafiek)
    # The optimal cut off would be where tpr is high and fpr is low
    # tpr - (1-fpr) is zero or near to zero is the optimal cut off point

    # for classifiers with decision_function, this achieves similar results
    # y_scores = classifier.decision_function(X_test)

    # fpr, tpr, thresholds = roc_curve(voorspellingen['incident'], voorspellingen['voorspelling_kans'])
    # roc_auc = auc(fpr, tpr)
    # print("Area under the ROC curve : %f" % roc_auc)
    #
    # i = np.arange(len(tpr))  # index for df
    # roc = pd.DataFrame(
    #     {'fpr': pd.Series(fpr, index=i), 'tpr': pd.Series(tpr, index=i), '1-fpr': pd.Series(1 - fpr, index=i),
    #      'tf': pd.Series(tpr - (1 - fpr), index=i), 'thresholds': pd.Series(thresholds, index=i)})
    # roc.iloc[(roc.tf - 0).abs().argsort()[:1]]
    #
    # print('roc:', roc)
    #
    # # Plot tpr vs 1-fpr
    # fig, ax = pl.subplots()
    # pl.plot(roc['tpr'])
    # pl.plot(roc['1-fpr'], color='red')
    # pl.xlabel('1-False Positive Rate')
    # pl.ylabel('True Positive Rate')
    # pl.title('Receiver operating characteristic')
    # ax.set_xticklabels([])

    return voorspellingen


def Find_Optimal_Cutoff(target, predicted):
    """ Find the optimal probability cutoff point for a classification model related to event rate
    Parameters
    ----------
    target : Matrix with dependent or target data, where rows are observations

    predicted : Matrix with predicted data, where rows are observations

    Returns
    -------
    list type, with optimal cutoff value

    """
    fpr, tpr, threshold = roc_curve(target, predicted)
    i = np.arange(len(tpr))
    roc = pd.DataFrame({'tf': pd.Series(tpr - (1 - fpr), index=i), 'threshold': pd.Series(threshold, index=i)})
    roc_t = roc.iloc[(roc.tf - 0).abs().argsort()[:1]]

    return list(roc_t['threshold'])



def performance(voorspellingen, trainset):

    '''''
    Functie berekent hoe veel incidenten in de top X (top X met hoogste kans op incident) goed zijn voorspeld tov totaal incidenten
    Dus als je naar bijv de top 10 kijkt met de hoogste kans op incident volgens het model: hoeveel van deze voorspellen we goed als incident
    
    Voorbeeld:
    Top 5 locaties op bijvoorbeeld 01-01-2020 om 15.00 met hoogste voorspelde kans op incident
    0   incident voorspeld              er was daadwerkelijk een incident           totaal 6 incidenten op dit moment
    1   incident voorspeld              er was daadwerkelijk geen incident          totaal 6 incidenten op dit moment
    2   incident voorspeld              er was daadwerkelijk geen incident          totaal 6 incidenten op dit moment
    3   geen incident voorspeld         er was daadwerkelijk een incident           totaal 6 incidenten op dit moment
    4   geen incident voorspeld         er was daadwerkelijk een incident           totaal 6 incidenten op dit moment

    In bovenstaand voorbeeld waren er totaal 6 incidenten, waarvan 1 goed voorspeld in de top 5 (20% correct), 
    2 van de 5 zijn onterecht als incident bestempeld (40%) en 2 van de 5 zijn onterecht als geen incident bestempeld
    
    '''''

    # Vul hierin welke top je wil bekijken (nu top 10)
    top = 10

    # Bepaal of het incident, ook voorspeld is als incident
    voorspellingen['incident_correct_voorspeld'] = np.where((voorspellingen.incident==1) & (voorspellingen.voorspelling_incident_optimal==1), 1, 0)
    voorspellingen['incident_incorrect_voorspeld'] = np.where((voorspellingen.incident == 0) & (voorspellingen.voorspelling_incident_optimal == 1), 1, 0)


    # Bereken totaal aan incidenten op dag-2uurblok en map die in de dataset
    # (gebruiken om te kijken of groot aantal incidentne in top X zit ja/nee)
    groupby_columns = ['Melding_dtm', 'Melding_tijd_2uur']
    aggregations = {'incident': 'sum'}
    voorspellingen_dag_2uur = voorspellingen.groupby(by=groupby_columns).agg(aggregations).reset_index()
    voorspellingen_dag_2uur.rename(columns={'incident': 'totaal_incidenten_dag2uur'}, inplace=True)

    # Voeg totaal incidenten dag 2 uur toe aan de voorspelset
    voorspellingen = pd.merge(voorspellingen, voorspellingen_dag_2uur, on=['Melding_dtm', 'Melding_tijd_2uur'], how='left')

    # Sorteer op grootste kans op incidenten per dag-2uurblok
    voorspellingen_sort = voorspellingen.sort_values(['Melding_dtm','Melding_tijd_2uur','voorspelling_kans'], ascending = (True, True, False))

    # Selecteer per dag per 2uurblok top X met grootste kans op incident
    voorspellingen_top= voorspellingen_sort.groupby(['Melding_dtm','Melding_tijd_2uur']).head(top)

    # Bereken per dag 2 uurblok het totaal correct in top X, incorrect top X en werkelijk aantal incidenten op de dag-2uurblo:
    groupby_columns = ['Melding_dtm', 'Melding_tijd_2uur']
    aggregations = {'incident_correct_voorspeld': 'sum',
                    'incident_incorrect_voorspeld': 'sum',
                    'voorspelling_kans': 'mean',
                    'totaal_incidenten_dag2uur': 'min'}
    voorspellingen_top_tot = voorspellingen_top.groupby(by=groupby_columns).agg(aggregations).reset_index()

    # Bereken % correct: hoeveel incidenten in top X goed tov totaal aantal incidenten
    voorspellingen_top_tot['aantal_incidenten_correct_in_top'] = np.where(voorspellingen_top_tot.totaal_incidenten_dag2uur==0, 1,
                                                                            voorspellingen_top_tot.incident_correct_voorspeld/
                                                                            voorspellingen_top_tot.totaal_incidenten_dag2uur)


    # Bepaald de top X stations met de meeste incidenten in trainset (baseline)

    # Bereken totaal aan incidenten per station
    groupby_columns = ['Locatie_cde']
    aggregations = {'incident': 'sum'}
    stations_totaal_incidenten = trainset.groupby(by=groupby_columns).agg(aggregations).reset_index()
    stations_totaal_incidenten.rename(columns={'incident': 'totaal_incidenten'}, inplace=True)

    # Sorteer op hoogste aantal incidenten per locatie
    stations_totaal_incidenten_sort = stations_totaal_incidenten.sort_values(['totaal_incidenten'], ascending=False)

    # Selecteer top X stations met meeste incidenten
    stations_top = stations_totaal_incidenten_sort.groupby(['Locatie_cde']).head(top)
    print('Stations met meeste incidenten: ',stations_top.Locatie_cde.head(top).values)
    stations_namen_top = stations_top.Locatie_cde.head(top).values

    # Filter voorspellingen op de top X stations met meeste incidenten (baseline)
    voorspellingen_top_10_stations_fixed = voorspellingen[voorspellingen.Locatie_cde.isin(stations_namen_top)]

    print('Gemiddeld aantal voorspelling goed in top ', top ,np.sum(voorspellingen_top_tot['incident_correct_voorspeld'])/np.sum(voorspellingen_top_tot['totaal_incidenten_dag2uur']))
    print('Gemiddeld aantal voorspelling goed in top ', top ,' fixed',np.sum(voorspellingen_top_10_stations_fixed['incident'])/np.sum(voorspellingen_top_tot['totaal_incidenten_dag2uur']))

    return



def model_incidents(df_incidenten_datasources):
    """
        De data wordt voorbereid voor het model (train/test) en model wordt aangeroepen:
        1. Data splitten in train, validatie en testset
        2. Model wordt getuned??
        3. ....

            Input:
            df_incidenten_datasources

            Returns:
            Nog in te vullen
        """

    # Filter op tilburg en den bosch (afgesproken met NS)
    #df_incidenten_datasources = df_incidenten_datasources[df_incidenten_datasources.Locatie_cde.isin(['Tb', 'Ht'])]
    #df_incidenten_datasources = df_incidenten_datasources[df_incidenten_datasources.Locatie_cde.isin(['Tb'])]

    df_incidenten_datasources = encode_category_variables(df_incidenten_datasources)

    trainset, testset = split_data(df_incidenten_datasources)


    voorspellingen = run_logistic_regression(trainset, testset)

    #Aanzetten als je voorspellingen niet opnieuw wil runnen
    #voorspellingen = pd.read_csv(os.path.join(OUTPUT_DATA, 'voorspellingen.csv'), sep=';')

    performance(voorspellingen, trainset)


    return


if __name__ == "__main__":

    df_incidenten_datasources = pd.read_csv(os.path.join(PREPROCESSED_DATA, 'Datasources/incidenten_cbs_kal_event.csv'), sep=';')
    model_incidents(df_incidenten_datasources)