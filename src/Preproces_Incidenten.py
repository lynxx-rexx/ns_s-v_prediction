import pandas as pd
import os
from datetime import timedelta
import numpy as np

from src.settings import INPUT_DATA, RAW_DATA, PREPROCESSED_DATA, OUTPUT_DATA

""" Script om incidenten data in te laden en de volgende dimensies toe te voegen:
    1. Melding_Hoofdsrt_cde
    2. Hoofdsoort_oms
    3. Locatie_nam
    4. Locatie_soort_oms
    
    De incidenten data wordt verder in dit script gefilterd, geaggregeerd en uitgebreid (momenten met 0 incidenten worden toegevoegd).
    
    output: df_incidenten_full
"""

def load_incidenten(file_in_incidenten):

    """ Ruwe incidenten data laden en benodigde kolommen overhouoden

        Input:
        file_in_incidenten: VC4_Meldingen.csv

        Returns:
        df_incidenten
    """
    df_incidenten_pre = pd.read_csv(file_in_incidenten, delimiter=",")

    columns_to_keep = ['Melding_dtm', 'Melding_tyd', 'Melding_Srt_cde', 'Locatie_cde', 'Locatie_Soort_cde', 'Melding_id']
    df_incidenten = df_incidenten_pre[columns_to_keep]

    # Filter stations eruit nvt: lelystad Zuid bestaat nog niet en antwerpen eruit
    df_incidenten = df_incidenten[~df_incidenten.Locatie_cde.isin(['Llsz','Atw'])]

    # Delft zuid = delft campus geworden in 2019
    df_incidenten['Locatie_cde'] = np.where(df_incidenten['Locatie_cde']=='Dtz', 'Dtcp', df_incidenten['Locatie_cde'])

    return df_incidenten


def load_meldingssoort_code(file_in_meldingssoort_code):

    """ Meldingsoort_code laden + benodige kolommen overhouden

            Input:
            file_in_meldingsoort_code: VC4_Meldingssoort_codes.csv

            Returns:
            df_meldingssoort_code
    """
    df_meldingssoort_code_pre = pd.read_csv(file_in_meldingssoort_code, delimiter=",")

    columns_to_keep = ['Melding_Hoofdsrt_cde', 'Melding_srt_cde']
    df_meldingssoort_code = df_meldingssoort_code_pre[columns_to_keep]
    df_meldingssoort_code = df_meldingssoort_code.rename(columns={"Melding_srt_cde": "Melding_Srt_cde"})

    return df_meldingssoort_code


def load_meldingssoort_hoofdcode(file_in_meldingssoort_hoofdcode):

    """ Meldingsoort_code laden + benodige kolommen overhouden

            Input:
            file_in_meldingsoort_hoofdcode: VC4_Meldingssoort_Hoofdcodes.csv

            Returns:
            df_meldingssoort_hoofdcode
    """

    df_meldingssoort_hoofdcode_pre = pd.read_csv(file_in_meldingssoort_hoofdcode, delimiter=",")

    columns_to_keep = ['Hoofdsoort_cde', 'Hoofdsoort_oms']

    df_meldingssoort_hoofdcode = df_meldingssoort_hoofdcode_pre[columns_to_keep]
    df_meldingssoort_hoofdcode = df_meldingssoort_hoofdcode.rename(columns={"Hoofdsoort_cde": "Melding_Hoofdsrt_cde"})

    return df_meldingssoort_hoofdcode


def load_locatie_codes(file_in_locatie_codes):

    """ Locatie_code laden + benodige kolommen overhouden

            Input:
            file_in_locatie_code: TB_NSVIP_v1_Locatie_codes.csv

            Returns:
            df_locatie_codes
    """

    df_locatie_codes_pre = pd.read_csv(file_in_locatie_codes, delimiter=",")
    df_locatie_codes_pre.fillna({'Risico_index_cde': 'F', 'Risico_rang_nmr': 375}, inplace=True)

    columns_to_keep = ['Locatie_cde', 'Locatie_nam', 'Buitenland_ind', 'Risico_index_cde', 'Risico_rang_nmr', 'Prorailtype', 'Bemenst_Station_ind']
    df_locatie_codes = df_locatie_codes_pre[columns_to_keep]

    return df_locatie_codes


def load_locatie_soort_codes(file_in_locatie_soort_codes):

    """ Locatie_soort_code laden + benodige kolommen overhouden

            Input:
            file_in_locatie_code: TB_NSVIP_Locatie_Soort_codes.csv.csv

            Returns:
            df_locatie_soort_codes
    """

    df_locatie_soort_codes_pre = pd.read_csv(file_in_locatie_soort_codes, delimiter=",")

    columns_to_keep = ['Locatie_Soort_cde' , 'Locatie_soort_oms']
    df_locatie_soort_codes = df_locatie_soort_codes_pre[columns_to_keep]

    return df_locatie_soort_codes


def add_dimensions_to_incidenten(df_incidenten, df_meldingssoort_code, df_meldingssoort_hoofdcode, df_locatie_codes,
                                 df_locatie_soort_codes, file_raw_incidenten, save=True):

    """ Meldinghoofdsoort, locatie en locatie_soort_oms toevoegen aan df_incidenten

            Input:
            df_incidenten, df_meldingssoort_code, df_meldingssoort_hoofdcode

            Returns:
            df_incidenten_join
    """

    df_incidenten_join_pre = df_incidenten.merge(df_meldingssoort_code, how='left')
    df_incidenten_join_pre1 = df_incidenten_join_pre.merge(df_meldingssoort_hoofdcode, how='left')
    df_incidenten_join_pre2 = df_incidenten_join_pre1.merge(df_locatie_codes, how='left')
    df_incidenten_join = df_incidenten_join_pre2.merge(df_locatie_soort_codes, how='left')

    if save:
        df_incidenten_join.to_csv(file_raw_incidenten, index=False, sep=';')
    return df_incidenten_join


def filter_incidents(df_incidenten_join, file_preprocess_incidenten_filter, save=True):

    """ df_incidenten_join wordt gefilterd op aggressieve incidenten (afgestemd met Paul van der Beek) en op
        locatie Station (=Locatie_soort_oms = 'Station')

        Aggressieve indicenten (alle meldingen zijn gedaan door het personeel):
        Melding hoofdsoort code 2 = Overlast
        Melding hoofdsoort code 3 = Aggressie tegen medewerkers Nb: betreft het incidenten waarbij 1 of meerdere medewerkers agressie hebben meegemaakt.
        Melding hoofdsoort code 4 = Agressie tegen reizigers
        Melding hoofdsoort code 6 = Vandalisme

            Input:
            df_incidenten_join

            Returns:
            df_incidenten_filtered
    """

    melding_codes_aggressie = [2, 3, 4, 6]
    df_incidenten_aggressie = df_incidenten_join[df_incidenten_join.Melding_Hoofdsrt_cde.isin(melding_codes_aggressie)]
    df_incidenten_filtered = df_incidenten_aggressie[df_incidenten_aggressie.Locatie_soort_oms=='Station']

    if save:
        df_incidenten_filtered.to_csv(file_preprocess_incidenten_filter, index=False, sep=';')

    return df_incidenten_filtered

def aggregate_incidents(df_incidenten_filtered, file_preprocess_incidenten_aggr, save=True):
    """
    df_incidenten_filtered wordt geaggregeerd per dag - station - twee uurblok: aantal incidenten
    Nieuw veld twee uurblok creeeren op basis van timestamp

        Input:
        df_incidenten_filtered

        Returns:
        df_incidenten_aggregate
    """
    # Converteer naar date en timestamp
    df_incidenten_filtered['Melding_dtm'] = pd.to_datetime(df_incidenten_filtered['Melding_dtm'], format='%d-%m-%Y %H:%M:%S').dt.date
    df_incidenten_filtered['Melding_tijd'] = pd.to_datetime(df_incidenten_filtered['Melding_tyd'], format='%d-%m-%Y %H:%M:%S')

    # Maak van timestamp een 2 uurblok -> rond tijd af op uurblok, als uurblok oneven is (bijvoorbeeld 3 uur) trek er dan 1 uur
    # vanaf, resultaat wordt dan 2 uur ipv 3 uur. Dit resulteert er in dat uurblok 2, zowel uurblok 2 als uurblok 3 bevat.
    df_incidenten_filtered['Melding_tijd_uur'] = df_incidenten_filtered['Melding_tijd'].dt.floor("H")
    df_incidenten_filtered['Melding_tijd_only_uur'] = df_incidenten_filtered['Melding_tijd'].dt.floor("H").dt.hour
    df_incidenten_filtered['Melding_tijd_2uur'] = np.where(df_incidenten_filtered['Melding_tijd_only_uur'] % 2  == 0,
                                                           df_incidenten_filtered['Melding_tijd_uur'].dt.time
                                                           , (df_incidenten_filtered['Melding_tijd_uur'] - timedelta(hours=1)).dt.time)

    # Aggregeer incidenten naar datum - 2uurblok - locatie
    groupby_columns = ['Melding_dtm', 'Melding_tijd_2uur', 'Locatie_cde', 'Locatie_nam', 'Buitenland_ind', 'Risico_index_cde', 'Risico_rang_nmr', 'Prorailtype', 'Bemenst_Station_ind']
    aggregations = {'Melding_id': 'count'}
    df_incidenten_aggregate = df_incidenten_filtered.groupby(by=groupby_columns).agg(aggregations).reset_index()

    new_names = {'Melding_id': 'aantal_incidenten'}
    df_incidenten_aggregate.rename(columns=new_names, inplace=True)

    if save:
        df_incidenten_aggregate.to_csv(file_preprocess_incidenten_aggr, index=False, sep=';')

    return df_incidenten_aggregate


def make_full_set_incidents(df_incidenten_aggregate, file_preprocess_incidenten_full, save=True):
    """
    df_incidenten_aggregate bevat alleen een record als er een incident is geweest.
    df_incidenten_aggregate wordt daarom uitgebreidt, zodat dat elke dag-2uurblok combinatie voorkomt voor een locatie.
    Er worden dus regels toegevoegd waar aantal incidenten = 0

        Input:
        df_incidenten_aggregate

        Returns:
        df_incidenten_full
    """

    #Wat als een datum niet voorkomt?

    # Maak alle dates, tijden, en stations uniek en maak daarna een outerjoin zodat alle combinaties voorkomen
    distinct_dates = pd.DataFrame(df_incidenten_aggregate.Melding_dtm.unique())
    distinct_dates.rename(columns={0: 'Melding_dtm'}, inplace=True)

    distinct_times = pd.DataFrame(df_incidenten_aggregate.Melding_tijd_2uur.unique())
    distinct_times.rename(columns={0: 'Melding_tijd_2uur'}, inplace=True)

    distinct_stations = pd.DataFrame(df_incidenten_aggregate[['Locatie_cde', 'Locatie_nam', 'Buitenland_ind', 'Risico_index_cde', 'Risico_rang_nmr', 'Prorailtype', 'Bemenst_Station_ind']].drop_duplicates())

    # Maak kolom om op te joinen
    distinct_dates['tmp'] = 1
    distinct_times['tmp'] = 1
    distinct_stations['tmp'] = 1

    # Outer join - alle combinaties maken
    distinct_dates_times = pd.merge(distinct_dates, distinct_times, on=['tmp'], how='outer')
    distinct_dates_times_stations = pd.merge(distinct_dates_times, distinct_stations, on=['tmp'], how='outer')
    distinct_dates_times_stations = distinct_dates_times_stations.drop('tmp', axis=1)

    # Join op incidenten set
    df_incidenten_full = pd.merge(df_incidenten_aggregate, distinct_dates_times_stations
                                       , on=['Melding_dtm','Melding_tijd_2uur','Locatie_cde', 'Locatie_nam', 'Buitenland_ind', 'Risico_index_cde', 'Risico_rang_nmr', 'Prorailtype', 'Bemenst_Station_ind'], how='outer')

    # Vul nan values op met 0 (=0 incidenten)
    df_incidenten_full = df_incidenten_full.fillna(0)

    # Sorteer dataset
    df_incidenten_full = df_incidenten_full.sort_values(by=['Locatie_cde', 'Locatie_nam', 'Melding_dtm', 'Melding_tijd_2uur'])

    if save:
        df_incidenten_full.to_csv(file_preprocess_incidenten_full, index=False, sep=';')

    # Check hoeveel % van de momenten er minstens 1 incident plaats vindt, gebruiken om te kijken hoevaak er 0 incidenten zijn
    #df_incidenten_full['ind_incident'] = np.where(df_incidenten_full.aantal_incidenten>0, 1, 0)
    #df_incidenten_full_groupby = df_incidenten_full.groupby(by=['Locatie_nam']).agg({'ind_incident': 'mean'}).reset_index()
    #print(df_incidenten_full_groupby)

    return df_incidenten_full

def make_binary_incidents(df_incidenten_full):
    """
        Het aantal incidenten wordt omgezet naar wel(1)/geen(0) incident kolom. Dit is nodig voor het logistic voorspelmodel.
        Meer dan 1 incident komt ook zelden voor dus je gooit niet veel belangrijke data weg

            Input:
            df_incidenten_full

            Returns:
            df_incidenten_full
    """

    df_incidenten_full['incident'] = np.where(df_incidenten_full['aantal_incidenten']>0, 1, 0)


    return df_incidenten_full


def create_lags(df_incidenten_full, lags):
    """
        Voeg historische lags toe aan incidenten data, deze hebben een voorspellende waarde

            Input:
            df_incidenten_full, lags

            Returns:
            df_incidenten_full_lags
        """


    for lag in lags:
        df_lags = df_incidenten_full.copy()
        week = int(lag / 7)
        column_name = 'Melding_dtm_last' + str(week) + 'week'
        df_incidenten_full[column_name] = df_incidenten_full['Melding_dtm'] - pd.to_timedelta(lag, unit='D')

        new_names = {'Melding_dtm': 'Melding_dtm_last' + str(week) + 'week',
                     'incident': 'incident_last' + str(week) + 'week'}

        df_lags.rename(columns=new_names, inplace=True)

        df_lags = df_lags[['Melding_tijd_2uur', 'Locatie_cde', 'Locatie_nam',
                           'Melding_dtm_last' + str(week) + 'week',
                           'incident_last' + str(week) + 'week']]

        # Left join op basis van data kolom veld, df_incidenten_full heeft datum-7 en df_lags datum, hierdoor krijg
        # je data van 1 week terug gejoined
        df_incidenten_full = pd.merge(df_incidenten_full, df_lags,
                      on=[column_name, 'Melding_tijd_2uur', 'Locatie_cde', 'Locatie_nam'],
                      how='left')

    df_incidenten_full_lags = df_incidenten_full.copy()
    df_incidenten_full_lags = df_incidenten_full_lags.dropna()

    df_incidenten_full_lags['weekdag'] = pd.to_datetime(df_incidenten_full_lags['Melding_dtm'], format='%Y-%m-%d').dt.day_name()



    return df_incidenten_full_lags


def create_incident_variables(df_incidenten_full_lags, file_preprocess_incidenten_full_lags, save=True):
     """
     Bereken statistieken van incidenten van station
     """
     groupby_columns = ['Locatie_cde'] # 'weekdag'
     aggregations = {'incident': 'sum',
                     'Melding_dtm': 'nunique'}
     df_incidenten_grouped = df_incidenten_full_lags.groupby(by=groupby_columns).agg(aggregations).reset_index()
     #stations_totaal_incidenten.rename(columns={'incident': 'totaal_incidenten'}, inplace=True)

     df_incidenten_grouped['gem_incidenten_per_dag'] = df_incidenten_grouped['incident']/df_incidenten_grouped['Melding_dtm']

     df_incidenten_final = pd.merge(df_incidenten_full_lags, df_incidenten_grouped[['Locatie_cde', 'gem_incidenten_per_dag']],on=['Locatie_cde'],how='left')


     #df_incidenten_final = df_incidenten_final[~ df_incidenten_final.Locatie_cde.isnull()]

     if save:
         df_incidenten_final.to_csv(file_preprocess_incidenten_full_lags, index=False, sep=';')

     return df_incidenten_final



def preprocess_incidenten():
    """
        De incidenten data wordt ingelezen, bewerkt en aggregeerd tot het gewenste format:
        1. laad incidenten data in en voeg dimensie informatie toe (stationnaam etc.)
        2. Filter incidenten data op juiste melding codes en op station locatie
        3. Aggregeer incidenten data per locatie-datum-2uurblok: sum incidenten
        4. Breidt de dataset uit, zodat dat elke dag-2uurblok combinatie voorkomt voor een locatie (of wel voeg regels toe met 0 incidenten)
        5. Voeg kolom toe met incidenten wel(1)/geen(0) (nodig voor logit model)
        6. Voeg historische lags toe  + dagtype
        7. Voeg incident variabelen toe (gemiddelde incident stalling per dag)

            Input:
            Incidenten data (VC4_Meldingen) uit NS database
            Dimensietabellen VC4_Meldingssoort_codes, VC4_Meldingssoort_Hoofdcodes, TB_NSVIP_v1_Locatie_codes, TB_NSVIP_Locatie_Soort_codes

            Returns:
            df_incidenten_full_lags
    """
    # Hoeveel dagen terug historie lags
    lags = (7,14,21,28)
    filepath_in = os.path.join(INPUT_DATA, 'Incidenten')
    filepath_raw = os.path.join(RAW_DATA, 'Incidenten')
    filepath_preprocess = os.path.join(PREPROCESSED_DATA, 'Incidenten')
    if not os.path.exists(filepath_in):
        os.makedirs(filepath_in)
    if not os.path.exists(filepath_raw):
        os.makedirs(filepath_raw)
    if not os.path.exists(filepath_preprocess):
        os.makedirs(filepath_preprocess)

    file_in_incidenten = os.path.join(filepath_in, 'VC4_Meldingen.csv')
    file_in_meldingssoort_code = os.path.join(filepath_in, 'VC4_Meldingssoort_codes.csv')
    file_in_meldingssoort_hoofdcode = os.path.join(filepath_in, 'VC4_Meldingssoort_Hoofdcodes.csv')
    file_in_locatie_codes = os.path.join(filepath_in, 'TB_NSVIP_v1_Locatie_codes.csv')
    file_in_locatie_soort_codes = os.path.join(filepath_in, 'TB_NSVIP_Locatie_Soort_codes.csv')

    # 1. laad incidenten data in en voeg dimensie informatie toe (stationnaam etc.)
    df_incidenten = load_incidenten(file_in_incidenten)
    df_meldingssoort_code = load_meldingssoort_code(file_in_meldingssoort_code)
    df_meldingssoort_hoofdcode = load_meldingssoort_hoofdcode(file_in_meldingssoort_hoofdcode)
    df_locatie_codes = load_locatie_codes(file_in_locatie_codes)
    df_locatie_soort_codes = load_locatie_soort_codes(file_in_locatie_soort_codes)

    file_raw_incidenten = os.path.join(filepath_raw, 'Incidenten.csv')
    df_incidenten_join = add_dimensions_to_incidenten(df_incidenten, df_meldingssoort_code, df_meldingssoort_hoofdcode
                                                      , df_locatie_codes, df_locatie_soort_codes, file_raw_incidenten)

    # 2. Filter incidenten data op juiste melding codes en op station locatie
    file_preprocess_incidenten_filter = os.path.join(filepath_preprocess, '1_Incidenten_aggressie_station.csv')
    df_incidenten_filtered = filter_incidents(df_incidenten_join, file_preprocess_incidenten_filter)

    # 3. Aggregeer incidenten data per locatie-datum-2uurblok: sum incidenten
    file_preprocess_incidenten_aggr = os.path.join(filepath_preprocess, '2_Incidenten_aggregate.csv')
    df_incidenten_aggregate = aggregate_incidents(df_incidenten_filtered, file_preprocess_incidenten_aggr)

    # 4. Breidt de dataset uit, zodat dat elke dag-2uurblok combinatie voorkomt voor een locatie (of wel voeg regels toe met 0 incidenten)
    file_preprocess_incidenten_full = os.path.join(filepath_preprocess, '3_Incidenten_full.csv')
    df_incidenten_full = make_full_set_incidents(df_incidenten_aggregate, file_preprocess_incidenten_full)

    #5. Voeg kolom toe met incidenten wel(1)/geen(0) (nodig voor logit model)
    df_incidenten_full = make_binary_incidents(df_incidenten_full)

    #6. Voeg historische lags toe + dagtype
    df_incidenten_full_lags = create_lags(df_incidenten_full, lags)

    #7. Voeg incident variabelen toe (gemiddelde incident stalling per dag)
    file_preprocess_incidenten_full_lags = os.path.join(filepath_preprocess, '4_Incidenten_full_lags.csv')
    df_incidenten_final = create_incident_variables(df_incidenten_full_lags, file_preprocess_incidenten_full_lags)

    return df_incidenten_final


if __name__ == "__main__":
    preprocess_incidenten()