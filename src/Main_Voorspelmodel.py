import pandas as pd
import os
from datetime import timedelta
import numpy as np


from src.settings import INPUT_DATA, RAW_DATA, PREPROCESSED_DATA, OUTPUT_DATA
from src.Preproces_Incidenten import preprocess_incidenten
from src.Combine_Datasources import combine_datasources
from src.Model_Incidents import model_incidents


def call_preprocess_incidents(herlaad=True):

    """ De incidenten data wordt ingelezen, bewerkt en aggregeerd tot het gewenste format.
        Er kan worden gekozen om de data te herladen of het output bestand van de laatste run in te lezen (dit scheelt run tijd)
    """

    if herlaad:
        print('Start preprocess incidenten')
        df_incidenten_full = preprocess_incidenten()
    else:
        print('Read file preprocess incidenten')
        df_incidenten_full = pd.read_csv(os.path.join(PREPROCESSED_DATA, 'Incidenten/4_Incidenten_full_lags.csv'), sep=';')

    return df_incidenten_full


def call_combine_datasources(df_incidenten_full, herlaad=True):

    """ De incidenten data wordt verrijkt met externe databronnen:
        - CBS data
        - Kalender met vakantie en feestdagen
        - ...

        Er kan worden gekozen om de data te herladen of het output bestand van de laatste run in te lezen (dit scheelt run tijd)
    """
    if herlaad:
        print('Start combine datasources')
        df_incidenten_datasources = combine_datasources(df_incidenten_full)

    else:
        print('Read file combine datasources')
        df_incidenten_datasources = pd.read_csv(os.path.join(PREPROCESSED_DATA, 'Datasources/incidenten_cbs_kal_event.csv'),sep=';')


    return df_incidenten_datasources

def call_model_incidents(df_incidenten_datasources, herlaad=True):

    """ De data wordt voorbereid voor het model (train/test) en model wordt aangeroepen

            Er kan worden gekozen om het model te herladen of het output model bestand van de laatste run in te lezen (dit scheelt run tijd)
    """

    if herlaad:
        print('Start model')
        final_model = model_incidents(df_incidenten_datasources)

    else:
        print('Read final model')
        #final_model = nog invullen


    return


if __name__ == "__main__":

    df_incidenten_full = call_preprocess_incidents(herlaad=True)
    df_incidenten_datasources = call_combine_datasources(df_incidenten_full, herlaad=True)

    call_model_incidents(df_incidenten_datasources, herlaad=True)

    # Moet nog gemaakt worden
    #call_predict_incidents()