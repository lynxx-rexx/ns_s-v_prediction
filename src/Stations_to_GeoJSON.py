import os
from shapely.geometry import Point
import geopandas as gpd
import pandas as pd


def save_geojson(folder, filename, gdf):
    # Store the geopandas to GeoJSON format in folder RAW\state:
    file_export = os.path.join(folder, filename)
    if not os.path.exists(folder):
        os.makedirs(folder)
    print('Exporting to: ' + file_export)
    # GM_NAAM & BU_NAAM can have special characters which is why the utf-8 enconding is necessary
    gdf.to_file(file_export, driver='GeoJSON', encoding='utf-8')  # als aternatief (iso-8859-1) # utf-8


def process_csv(file_path_in, folder_out, filename_out, radius=5, save=False):
    print('opening csv: {}'.format(file_path_in))
    df = pd.read_csv(file_path_in, delimiter=";", decimal=",", encoding='latin-1')

    geometry = [Point(xy) for xy in zip(df.X_Coordinaat_cde, df.Y_Coordinaat_cde)]
    df = df.drop(['X_Coordinaat_cde', 'Y_Coordinaat_cde'], axis=1)
    crs = {'init': 'epsg:4326'}
    gdf = gpd.GeoDataFrame(df, crs=crs, geometry=geometry)
    #print(gdf.head())
    # removing rows which have no geometry --> otherwise the functions don't work!
    gdf_not_null = gdf[gdf['geometry'].notnull()].copy()
    #degrees to meters: m / 111139
    gdf_not_null['geometry'] = gdf_not_null.buffer(radius/111.139)


    # Store the geopandas to GeoJSON format in folder RAW\state:
    if save:
        folder = folder_out
        # filename = shape_file[:-4] + '_' + str(tolerance) + '_.geojson'
        filename = filename_out
        save_geojson(folder, filename, gdf_not_null)

def station_to_geojson():
    folder_in = '../data/0_input/Stations'
    filename_in = 'stations.csv'
    file_path_in = os.path.join(folder_in, filename_in)
    folder_out ='../data/1_raw/Stations'
    filename_out = 'stations.geojson'
    file_path_out = os.path.join(folder_out, filename_out)

    process_csv(file_path_in, folder_out, filename_out, radius = 2.5, save=True)

    return

if __name__ == "__main__":
    station_to_geojson()




