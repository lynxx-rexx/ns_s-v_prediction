import os
from shapely.geometry import Point
import geopandas as gpd
import pandas as pd
import numpy as np

from src.CBS_to_GeoJSON import cbs_to_geojson
from src.Stations_to_GeoJSON import station_to_geojson


def load_stations(path_base):
    gdf = gpd.read_file(path_base)
    return gdf

def load_pc4(path_base):
    gdf = gpd.read_file(path_base)
    return gdf

def add_centroid_to_pc4(gdf):
    if not 'centroid' in gdf.columns:
        #print('\n' + 15 * '-' + ' Adding centroids ' + 15 * '-')
        # Create latitude and longitude
        gdf['centroid'] = gdf.centroid.copy()
        # Replace polygons with centroids
        gdf['geometry'] = gdf['centroid']
        gdf = gdf.drop(['centroid'], axis = 1)
        gdf_not_null = gdf[gdf['geometry'].notnull()].copy()

        #Change coordinates to lat-long (from rijksdriehoek)
        gdf_not_null = gdf_not_null.to_crs(epsg=4326)
        #print(gdf_not_null.head())
    return gdf_not_null

def create_station_df(gdf_station, gdf_pc4, save =True, save_preprocessed = True):
    # Check welke PC4's binnen station buffer vallen
    dfsjoin = gpd.sjoin(gdf_station, gdf_pc4)  # Spatial join Points to polygons

    # Vervang ontbrekende en verhulde gegevens door Nan, nodig voor aggregaties zodat deze niet meeberekend wordt
    dfsjoin = dfsjoin.replace(-99997, np.NaN)

    if save:
        if not os.path.exists('../data/2_preprocessed/CBS'):
            os.makedirs('../data/2_preprocessed/CBS')
        dfsjoin.to_csv('../data/2_preprocessed/CBS/cbs_station_pc4.csv')

    # Hier wil je eigenlijk je columns definiëren, maar lukt nog niet
    #columns_to_sum = ['INWONER', 'MAN']
    #columns_to_avg = ['GEM_HH_GR']
    #columns_to_keep = ['Locatie_cde'] + columns_to_sum + columns_to_avg

    # Filter dataframe op kolommen die je wilt houden, en stel de agg. functies in
    #final_df = dfsjoin[columns_to_keep]
    cbs_station = dfsjoin.groupby(['Locatie_cde']).agg({'INWONER': 'sum'
                                                    , 'MAN': 'sum'
                                                    , 'INW_014': 'sum'
                                                    , 'INW_1524': 'sum'
                                                    , 'INW_2544': 'sum'
                                                    , 'INW_4564': 'sum'
                                                    , 'TOTHH_EENP': 'sum'
                                                    , 'TOTHH_MPZK': 'sum'
                                                    , 'HH_EENOUD': 'sum'
                                                    , 'HH_TWEEOUD': 'sum'
                                                    , 'WONING': 'sum'
                                                    , 'WON_HCORP': 'sum'
                                                    , 'UITKMINAOW': 'sum'

                                                    , 'P_NW_MIG_A': 'mean'
                                                    , 'WOZWONING': 'mean'
                                                    , 'P_HUURWON': 'mean'
                                                    , 'AV3_CAFE': 'mean'
                                                    , 'AV3_CAFTAR': 'mean'
                                                    , 'AV5_HOTEL': 'mean'
                                                    , 'AV3_RESTAU': 'mean'
                                                    , 'AFS_OPRIT': 'mean'
                                                    , 'AFS_TREINS': 'mean'
                                                    , 'OAD': 'mean'
                                                    , 'STED': 'mean'
                                                    }).reset_index()

    cbs_station['Locatie_cde'] = cbs_station.Locatie_cde.str.capitalize()

    if save:
        cbs_station.to_csv('../data/2_preprocessed/CBS/cbs_station.csv', sep=';')

    return cbs_station

def combine_stations_cbs():
    cbs_to_geojson()
    station_to_geojson()

    file_path_stations = '../data/1_raw/Stations/stations.geojson'
    file_path_pc4 = '../data/1_raw/CBS2017/CBS_PC4_2017_v2.geojson'

    gdf_station = load_stations(file_path_stations)
    gdf_pc4 = load_pc4(file_path_pc4)

    gdf_pc4 = add_centroid_to_pc4(gdf_pc4)

    cbs_station = create_station_df(gdf_station, gdf_pc4, save=True, save_preprocessed=True)

    return cbs_station

if __name__ == "__main__":

    combine_stations_cbs()




#INWONER, INW_014, INW_1524, INW_2544,INW_4564, INW_65PL, P_NW_MIG_A, percentage

